package csv

import (
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/republico/library/go/extractor/internal/pkg/util"
	"gitlab.com/republico/library/go/util/pkg/file"
)

func ReadZipFile(pathFile string, pathUnzip string, separator string) (map[string][][]string, error) {
	pathUnzipAdjusted := util.GetPathDestinationName(pathFile, pathUnzip)

	file.Unzip(pathFile, pathUnzipAdjusted)
	csvList, err := readInternalFile(pathUnzipAdjusted, separator)
	if err != nil {
		return csvList, err
	}

	err = os.RemoveAll(pathUnzip)
	if err != nil {
		return map[string][][]string{}, err
	}

	return csvList, nil
}

func readInternalFile(path string, separator string) (map[string][][]string, error) {
	csvMap := make(map[string][][]string)

	fileInfo, err := ioutil.ReadDir(path)
	if err != nil {
		return csvMap, err
	}
	
    for _, f := range fileInfo {
		nameFile := f.Name()
		split := strings.Split(nameFile, ".")
		extension := split[len(split) - 1]
		keyArray := strings.Replace(nameFile, "." + extension, "", -1)

		var csvData [][]string
		if extension == "csv" {
			csvData, err = file.ReadCsv(path + nameFile, separator)
			if err != nil {
				return csvMap, err
			}
		}

		csvMap[keyArray] = csvData
    }

	return csvMap, nil
}