package csv

import (
	"gitlab.com/republico/library/go/util/pkg/file"
)

func ReadFile(pathFile string, separator string) ([][]string, error) {
	csvData, err := file.ReadCsv(pathFile, separator)
	return csvData, err
}