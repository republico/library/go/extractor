package file

import (
    "gitlab.com/republico/library/go/util/pkg/file"
)

func Download(url string, filePath string) error {
    err := file.Download(url, filePath)
    if err != nil {
        return nil
    }

    return nil
}