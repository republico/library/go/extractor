package test

import (
	"testing"

	"gitlab.com/republico/library/go/extractor/pkg/json"
)

func TestReadZipFile(t *testing.T) {
	pathFile := "C:/Teste/siconv_consorcios_teste_10.zip"
	pathUnzip := "C:/Teste/"
	separatorCsv := ";"

	jsonArray, _ := json.ReadZipFile(pathFile, pathUnzip, separatorCsv)
	if len(jsonArray) == 0 {
		t.Errorf("Dados não foram lidos.")
	}
}