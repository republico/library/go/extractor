package util

import (
	"path"
	"path/filepath"
	"strings"
)

func GetPathDestinationName(pathFile string, pathUnzip string) string {
	filename := filepath.Base(pathFile)

	if pathUnzip[len(pathUnzip) - 1:] != "/" {
		pathUnzip += "/"
	}

	filenameWithoutExtension := strings.TrimSuffix(filename, path.Ext(filename))

	return pathUnzip + filenameWithoutExtension + "/"
}